import Vue from 'vue';
import { Element, Connection } from './data'

interface GraphVertex {
    id: string
    activeIndex: number
    accepts: string
    provides: string
    // distance: number
}
// export interface Graph {
//     vertices: GraphVertex[]
//     score?: number
// }


export class Graph {
    // vertices: GraphVertex[] = []
    // activeInterpeters: string[] = []
    providers: string[] = []
    ignoredProviders: string[] = []
    constructor() {

    }
    clone(): Graph {
        let g = new Graph
        g.providers = this.providers.slice(0)
        g.ignoredProviders = this.ignoredProviders.slice(0)

        // g.activeInterpeters = this.activeInterpeters.slice(0)
        return g
    }
    activeConnections(allConnections: Connection[]): Connection[] {
        return allConnections.filter(c => {
            if(!this.providers.includes(c.from+':'+c.fromIndex)) return false

            // if not an output-only element
            if(!c.to.match(/^(tx|sign|pa)/)) {
                if(!this.providers.includes(c.to+':'+c.toIndex)) return false
            }
            return true
        })
    }
    calculateScore(elements: Element[]) {
        // consider:
        // * distance
        // * interpreter lang preference / priority
        // * language missing
        
    }
    toString() {
        return this.providers.sort().join('|')
    }
}
function cloneGraphs(ga: Graph[]) {
    return ga.map(g => g.clone())
}

export default new Vue({
    data() {
        return {
            elements: [] as Element[],
        }
    },
    computed: {
        elementsObj() {
            const obj: {[id: string]: Element} = {}
            for(let el of this.elements) {
                obj[el.id] = el
            }
            return obj
        },

        // 1. generate all possible connections (ignore stageLang providers already)
        //    O(n²)
        allPossibleConnections() {
            const connections: Connection[] = []

            const stage = this.elementsObj['stage']
            // if(!stage) throw new Error('do stage found. we cannot continue without')
            const stageLang = stage.langs[0].provides

            // interate over all possible inputs
            for(let provideEl of this.elements) {
                provideEl.langs.forEach( ({provides},fromIndex) => {
                    if(!provides) return
                    if(provideEl.id !== 'stage' &&  provides == stageLang) return // no one needs ever to provide the same lang as spoken on stage

                    // iterate over all possible outputs

                    for(let acceptEl of this.elements) {
                        if(acceptEl.id == provideEl.id) continue // don't connect to self

                        acceptEl.langs.forEach( (l,toIndex) => {                        
                            const accepts = l.accepts
                            if(!accepts) return
                            if(l.provides == stageLang)  {
                                // again: we don't need any input for a language no one will ever need to provide
                                return
                            } 
                            if(accepts == provides) {

                                const conn = {
                                    from: provideEl.id,
                                    to: acceptEl.id,
                                    fromIndex,
                                    toIndex,
                                    lang: provides
                                }
                                connections.push(conn)
                            }
                        })
                    }
                })
            }
            return connections
        },

        
        // 2. remove any cycles
        //    O(n²)
        // TODO! 
        // this algorithm is broken, removes sometimes non-cycling paths
        // Open Question: is it even possible to reliable remove cycles before creating the graphs?
        allNonCycleConnections() {
            let visitedIds: string[] = []
            let nextIds = ['stage:0']

            const connections: Connection[] = []
            while(nextIds.length) {
                let ids = nextIds
                nextIds = []
                for(let id of ids) {
                    if(visitedIds.includes(id)) {
                        // circle
                        continue
                    }
                    visitedIds.push(id)
                    const [from,index] = id.split(':')
                    const conns = this.allPossibleConnections.filter(c => c.from == from && c.fromIndex == parseInt(index))

                    for(let c of conns) {
                        let i = c.to+':'+c.toIndex
                        if(visitedIds.includes(i)) {
                            // circle!
                            console.log('circle', c, visitedIds)
                            continue
                        }
                        connections.push(c)
                        nextIds.push(i)
                    }

                }
                console.log('loop')
            }
            return connections
        },


        // 3. remove all dead ends
        //    O(?)
        allContinuousConnections() {
            let connections: Connection[] = [].concat(this.allNonCycleConnections as any)

            // run multiple times until every dead end got removed
            let hasRemovedAnyConnection = true
            while(hasRemovedAnyConnection) {
                hasRemovedAnyConnection = false
                connections = connections.filter(c1 => {
    
                    if(c1.to.match(/^(tx|sign|pa)/)) {
                        // don't exclude ends, which never have provide any language
                        return true 
                    }
                    const next = connections.find(c2 => c2.from == c1.to && c2.fromIndex == c1.toIndex)
                    if(!next) {
                        hasRemovedAnyConnection = true
                        return false
                    } else {
                        return true
                    }
                })
            }

            return connections
        },

        // 4. generate all possible gravs
        //     O(?)
        allPossibleGraphs() {
            const conns = this.allContinuousConnections


            let graphs: Graph[] = []
            
            // generate all graphs
            graphs.push(new Graph)
            for(let el of this.elements)  {
                const eligibleConns = conns.filter(c => c.from == el.id)
                const eligibleLangIndexes = eligibleConns.map(c => c.fromIndex).sort().filter((x, i, a) => !i || x != a[i-1])  // unique indexes

                let newGraphs = []
                if(eligibleLangIndexes.length) {
                    for(let index of eligibleLangIndexes) {
                        const ignored = eligibleLangIndexes.filter(i => i != index)
                        for(let g of graphs) {
                            g = g.clone()
                            g.providers.push(el.id+':'+index)
                            g.ignoredProviders.push(...ignored.map(i => el.id+':'+i))
                            newGraphs.push(g)
                        }
                    }
                    graphs = newGraphs
                } else {
                    for(let g of graphs) {
                        // g.ignoredElements.push()
                    }
                }
            }

            // filter dead ends
            for(let g of graphs) {
                for(let s of g.providers) {
                    const [elId,index] = s.split(':')
                    if(elId == 'stage') {
                        // the stage does not require an previous element
                        continue
                    }
                    const prevConns = this.allContinuousConnections.filter(c => c.to == elId && c.toIndex == parseInt(index))

                    let anyActivePreviousElement = false
                    for(let c of prevConns) {
                        if(g.providers.includes(c.from+':'+c.fromIndex)) {
                            anyActivePreviousElement = true
                            break
                        }
                    }
                    if(!anyActivePreviousElement) {
                        // remove from providers list
                        let pIndex = g.providers.indexOf(s)
                        g.providers.splice(pIndex, 1)
                    }
                }
            }

            // filter graph duplicates
            // -> possible due to connection removal
            const graphObject: {[str: string]: Graph} = {}

            for(let g of graphs) {
                graphObject[g.toString()] = g
            }
            return Object.values(graphObject)
        }
    }
})