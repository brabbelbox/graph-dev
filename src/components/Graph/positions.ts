import Vue from 'vue'
import * as d3 from 'd3-force'

import { Element, Connection } from './data'

let sim: d3.Simulation<any,any>|null = null
let simStopTimer: any

export default new Vue({
    data() {
        return {
            // following is used to calculate initial positions
            width: 100,
            height: 100,
            transmitters: [] as Element[],
            interpreters: [] as Element[],
            interpretersSign: [] as Element[],

            //-----
            variablePositions: {} as {[id: string]: {x: number, y: number}}
        }
    },
    computed: {
        fixedPositions() {
            const pos: any = {}
            pos.stage = {
                x: this.width/2,
                y: 40,
                fixed: true
            }

            // transmitter, PA
            let txIndex = 0
            let txY = this.height - 50;
            let txSpace = this.width / (this.transmitters.length + 1)
            for(let el of this.transmitters) {
                pos[el.id] = {
                    x: txSpace/1.5 + (txIndex++)*txSpace,
                    y: txY,
                    fixed: true
                }
            }
            
            return pos
        },
        pos() {
            return Object.assign({}, this.fixedPositions, this.variablePositions)
        },
    } as any,
    methods: {
        calculateInitPositions() {
            const yCenter = this.height/2
            let intIndex = 0
            let space = this.width / (this.interpreters.length+this.interpretersSign.length)
            for(let el of this.interpreters) {
                this.$set(this.variablePositions, el.id, {
                    x: space/2 + space*(intIndex++),
                    y: yCenter
                })
            }
        },
        forceSimulation(elements: Element[], connections: Connection[]) {
            if(simStopTimer) clearTimeout(simStopTimer)
            if(sim) sim.stop()

            const indexes : any = {}
            const nodes = elements.map((el,i) => {
                indexes[el.id] = i
                // @ts-ignore
                const pos = this.pos[el.id]
                if(pos.fixed) {
                    return {
                        index: i,
                        id: el.id,
                        fx: pos.x,
                        fy: pos.y
                    }
                } else {
                    return {
                        index: i,
                        id: el.id,
                        x: pos.x,
                        y: pos.y,
                        vx: 0,
                        vy: 0
                    }
                }
            })
            const links = connections.map(c => {
                return {
                    source: indexes[c.from],
                    target: indexes[c.to],
                }
            })
            sim = d3.forceSimulation(nodes)
                // .force("body", d3.forceManyBody())
                .force("collision", d3.forceCollide(100).strength(0.5))
                .force("link", d3.forceLink(links).strength(1))
                .force("x", d3.forceX(this.width/2).strength(0.2))
                .force("y", d3.forceY(this.height*0.6).strength(0.6))
                // .force("limitx", d3.forceY())
                // .force("center", d3.forceCenter(this.width/2,this.height/2))
                .on('tick', () => {
                    for(let node of nodes) {
                        if(node.fx) continue // don't update fixed elements
                        this.variablePositions[node.id].x = node.x
                        this.variablePositions[node.id].y = node.y
                    }
                })

            simStopTimer = setTimeout(( ) => {
                if(sim) sim.stop()
            }, 10000)
        }
    }
    
})