export interface LanguageTuple {
    accepts?: string
    provides?: string
    priority?: number
}

export interface Element {
    id: string
    langs: LanguageTuple[]
    label?: string
}

export interface Connection {
    from: string
    to: string
    fromIndex: number
    toIndex: number
    lang: string
    distance?: number
}

/*
    current graph includes
        * cycles
        * shortest path for ES would lead to an impossible situation for others
        * multiple interpreter can provide same languages simultaniously
        * unused interpreter
    
    TODO: add more edge cases!
*/
export const elements: Element[] = [
    {
        id: 'stage',
        langs: [
            {
                provides: 'DE'
            }
        ]
    },
    {
        id: 'int1',
        langs: [
            {
                accepts: 'EN',
                provides: 'DE',
                priority: 2,
            },
            {
                accepts: 'DE',
                provides: 'EN',
                priority: 2,
            },
            {
                accepts: 'DE',
                provides: 'ES',
                priority: 1
            }
        ]
    },
    {
        id: 'int2',
        langs: [
            {
                accepts: 'EN',
                provides: 'ES'
            },
            {
                accepts: 'ES',
                provides: 'DE'
            }
        ]
    },
    {
        id: 'int3',
        langs: [
            {
                accepts: 'DE',
                provides: 'FR'
            },
            {
                accepts: 'FR',
                provides: 'DE'
            }
        ]
    },
    {
        id: 'int4',
        langs: [
            {
                accepts: 'FR',
                provides: 'AR'
            },
            {
                accepts: 'EN',
                provides: 'AR'
            },
            {
                accepts: 'EN',
                provides: 'FR'
            }
        ]
    },
    {
        id: 'int5',
        langs: [
            {
                accepts: 'FR',
                provides: 'AR'
            },
            {
                accepts: 'AR',
                provides: 'FR'
            }
        ]
    },
    // {
    //     id: 'int6',
    //     langs: [
    //     ]
    // },
    {
        id: 'tx1',
        langs: [
            {
                accepts: 'EN'
            }
        ]
    },
    {
        id: 'tx2',
        langs: [
            {
                accepts: 'DE'
            }
        ]
    },
    {
        id: 'tx3',
        langs: [
            {
                accepts: 'ES'
            }
        ]
    },
    {
        id: 'tx4',
        langs: [
            {
                accepts: 'FR'
            }
        ]
    },
    {
        id: 'tx5',
        langs: [
            {
                accepts: 'AR'
            }
        ]
    },
    
]